#!/bin/python3
import serial

port = serial.Serial("/dev/ttyAMA0", baudrate=115200, timeout=3.0)

while True:
    port.write(str.encode(str(input("* ")) + "\r\n"))
    rcv = port.read(100)
    print("Result: " + bytes.decode(rcv))
    #port.write(repr(rcv))
